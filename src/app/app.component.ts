import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'My Todo';
  todo=[
  {
    label:'Bring Milk',
    done : false,
    priority : 3
  },
  {
    label:'Pay Mobile Bill',
    done : true,
    priority : 1
  },
  {
    label:'Clean House',
    done : false,
    priority : 4
  },
  {
    label:'Fix Bulb',
    done : false,
    priority : 5
  }
  ];
  addTodo(newTodolabel){

    var newTodo={
      label: newTodolabel,
    done : false,
    priority : 1
    };
    this.todo.push(newTodo);
  }

}
